# CoinOp

Simple cryptocurrency monitor. This is an experimental project on building APIs for cryptocurrencies.


## Setup

You'll need PostgreSQL and Redis already installed on your system. Please review the `settings` and make sure your environment has the proper configuration to run the app.

While within the main `coinop` directory:

```bash
pipenv install
createdb coinop
export FLASK_APP=app.py

# Open a new terminal to run the workers:
flask rq worker

# Open a new terminal to run the scheduler:
flask rq scheduler

flask run  # OR
python3 app.py
```

You can also create Supervisor configuration to run the worker and scheduler in the background. See `config/coinop.conf` for details.


### Dev Notes

__Ipython Shell__

TODO

__Cron Jobs__

Coinop is configured to automatically get coin updates once per day. The cron jobs are defined in `coinop/app.py`.

To cancel running jobs, open the Ipython shell and run the following:
```
from coinop.tasks import rq

schedule = rq.get_scheduler()
for job in scheduler.get_jobs(): scheduler.cancel(job)
```

## Resources

* Supervisord: http://supervisord.org/running.html


## TODO

- Pagination
- Caching
- Query options: offset
- Browsable api docs (might be simpler to roll a basic custom solution than to manage Swagger):
    * HTML templates (jinja)
    * API routes (docs.py)
    * Embed api responses in templates
- Tests (unittest?)
- Migrations
- Feature: interactive visualizations
- Feature: News API related to each coin
