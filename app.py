import os

from coinop import create_app
from coinop.tasks import daily_coins

config = os.getenv('FLASK_SETTINGS', 'coinop.settings.dev')
app = create_app(config)

config_file = ('COINOP_CONFIG_FILE'
               if not app.testing
               else 'COINOP_TEST_CONFIG_FILE')

# Start Tasks
daily_coins.cron('0 5 * * *', 'Daily Update')  # Every day at 5am

if __name__ == '__main__':
    # TODO: No longer needed?
    # app.config.from_envvar(config_file)

    if app.debug:
        from flask_debugtoolbar import DebugToolbarExtension
        # For HTML responses only!
        DebugToolbarExtension(app)
    app.run()
