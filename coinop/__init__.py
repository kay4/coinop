from flask import Flask

from werkzeug.utils import import_string, find_modules

from coinop import views
from coinop.models import db
from coinop.serializers import ma
from coinop.settings import common
from coinop.tasks import rq, daily_coins
from coinop.views import auth, coins


def create_app(config_name):
    _app = Flask(__name__)
    _app.config.from_object(config_name)
    db.app = _app
    register_all_blueprints(_app)
    rq.app = _app
    db.init_app(_app)
    ma.init_app(_app)
    rq.init_app(_app)
    _app.config.broker_url = 'redis://localhost:6379/0'
    db.create_all()

    return _app


def register_all_blueprints(app):
    """
    Searches the Views directory for blueprints and registers them.

    All views must have a `bp` attribute whose value is a Blueprint(). Ex.:
        `bp = Blueprint('my_view', __name__)`

    It will not be registered with the current application unless it has
    this attribute!

    :param app: The application for registration
    :return:
    """
    for view in find_modules('coinop.views'):
        mod = import_string(view)
        uniq = set()
        if hasattr(mod, 'bp'):
            # Avoid duplicate imports
            if mod not in uniq:
                uniq.add(mod)
                app.register_blueprint(mod.bp)
    app.logger.info("Blueprints registered.")


# Configure routes
# app.register_blueprint(auth.auth_bp)
# app.register_blueprint(coins.coin_bp)
