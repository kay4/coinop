import datetime as dt

from flask import Blueprint, current_app, jsonify, render_template

from coinop.serializers import coin_schema, Coin

bp = Blueprint('coins', __name__)


@bp.route('/')
def home():
    return render_template('coins/index.html')


@bp.route('/api/latest')
def latest_coins():
    current_app.logger.info("Fetching latest coin prices ...")
    # Fetch from db because the job.result will not be immediately available!
    coins = coin_schema.dump(Coin.query.filter(Coin.date >= dt.date.today()), many=True)
    current_app.logger.info(f"--> Coin count: {len(coins)}")
    return jsonify(coins)


@bp.route('/api/subscribe')
def subscribe_to_coin_updates():
    pass
